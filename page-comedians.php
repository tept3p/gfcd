<?php
/**
 * Template Name: Comedians
 
 */

get_header(); ?>

<div class="main-content home">
         
    <?php
        /* Run the loop to output the page.
        * If you want to overload this in a child theme then include a file
        * called loop-page.php and that will be used instead.
        */
        get_template_part( 'loop', 'page' );
    ?>
    
    <div id="comedians">
        
         <?php	
                $page = $post->post_name;
                
                if($page == 'awards') { ?>
                    <aside id="belinda">
                        <img src="http://grandfinalcomedydebate.com.au/files/2012/08/belinda-duarte-thumb.jpg" alt="Hamish McLachlan" />
                        <label><a href="http://grandfinalcomedydebate.com.au/belinda-duarte/" title="Belinda Duarte">2012 Winner<br />Belinda Duarte</a></label>
                        
                    </aside>
        
                    <h3 style="margin-left:10px; color:#F185B6;">2012 Finalists</h3>
                    
                <?php }
                
                
                query_posts(array('category_name' => $page, 'caller_get_posts' => 1, 'orderby' => 'rand', 'order' => 'DESC',));
            
                if (have_posts()) : while (have_posts()) : the_post(); 
                ?> 
        
                <?php if($post->post_name == 'belinda-duarte') { 
                    
                    
                } else { ?> 
                    <article>
                        <a href="<?php echo get_permalink($post->ID); ?>">
                            <?php the_post_thumbnail(); ?>
                            
                            <label><?php echo the_title(); ?></label>
                        </a>                  
                    </article>
                <?php } ?>

                <?php endwhile; ?>
                <?php else : ?>
                <h5>No posts were found.</h5>
                <?php endif; ?>
                
                <?php
                if(is_category('comedians')) {
                ?>    
             
                <aside id="hamish">
                    <img src="<?php bloginfo('template_directory'); ?>/images/Hamish-McLachlan-Large.jpg" alt="Hamish McLachlan" />
                    <label>Hamish McLachlan<br /> MC and Debate moderator</label>
                    <p>Australian sports broadcaster currently employed with the Seven Network and Austereo.</p>
                </aside>
                
                <?php } ?> 
               
                <?php wp_reset_query(); ?>
       
              
     </div>
        
                     
</div> 

    

		

<?php get_footer(); ?>
