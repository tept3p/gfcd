<?php
/**
 * Template Name: Raffle
**/

get_header(); ?>

<div class="main-content live-auction">
         
    <?php
        /* Run the loop to output the page.
        * If you want to overload this in a child theme then include a file
        * called loop-page.php and that will be used instead.
        */
        get_template_part( 'loop', 'page' );
    ?> 
    
<!--    <div class="tab-container left-tab">	
    <h3 class="right-header"><?php //echo 'Auction Items - ' . ucwords($category); ?></h3>
    <div class="med-right-tab right-tab"></div>
    </div>-->

    <div class="live-cat">
<!--        <a href="/silent-auctions" class="back-btn">Back to Categories</a>-->
        
     
         <?php	
                
        
        query_posts(array(  'post_type' => 'auction_items', 'caller_get_posts' => 1,  'auction_item' => 'raffle', 'posts_per_page' => -1,  'order' => 'ASC'));
        
                                                                                                                                       
                if (have_posts()) : while (have_posts()) : the_post(); 
                
                $custom_field =  get_post_meta($post->ID);
                
                ?> 
        
                    <article>
                        <h4><?php echo the_title(); ?></h4>
                        <p><?php echo the_content(); ?></p>
                            
                        <label>RRP $<?php echo $custom_field['rrp'][0] ?></label>
                                          
                    </article>
                
                        
                <?php endwhile; ?>
                <?php else : ?>
                <h5>No posts were found.</h5>
                <?php endif; ?>
         
         
         

       
           <a href="/auction" class="back-btn">Back to Auction</a>   
     </div>
        
                     
</div> 

    

		

<?php get_footer(); ?>
