<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
 <?php wp_title(' '); ?>
 | Grand Final Comedy Debate
 </title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/jquery.fancybox-1.3.4.css" />
<link rel="icon" 
      type="image/png" 
      href="<?php bloginfo('template_directory'); ?>/images/favicon16.png">

<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34568940-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!-- jquery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.backstretch.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.fancybox-1.3.4.js"></script>
<script>
    $.backstretch("<?php bloginfo('template_directory'); ?>/images/background.jpg");
    
    $(document).ready(function(){
        $(".gallery br").remove();
        $(".gallery-item a").attr("rel","group");
        $(".gallery-item a").fancybox({
            'padding'       : 10,
            'showNavArrows' : true,
            'overlayColor'  : '#000',
            'transitionIn'  : 'fade',
            'transitionOut' : 'fade',
            'titleShow'     : false
        });
    });
</script>

<?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>


    <header id="access" role="navigation" class="pink">
        <nav class="container_12">
        <?php /* Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
            <?php /* Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu. The menu assiged to the primary position is the one used. If none is assigned, the menu with the lowest ID is used. */ ?>
            <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
        </nav>    
    </header> 
    

<section class="container_12">
    
    <div class="side-content">
        <hgroup>
            <h1><a href="/home" title="Book Grand Final Comedy Debate Tickets with Ticketmaster" target="_blank">The Grand Final Comedy Debate</a></h1>
<!--            <h3>September 27, 2012</h3>
            <h3>11.30am - 3.00pm</h3>
            <h3>Crown Palladium</h3>-->

        </hgroup>
        <div class="long-tab-container">
            <h2 class="tickets">September 26th, 2013<br />Crown Palladium<br /><span>Bookings will open on Wednesday 1st May</span></h2>
            <div class="long-right-tab"></div>
        </div>
        
        <a href="http://twitter.com/home?status=Currently reading http://grandfinalcomedydebate.com.au/" title="Click to share this post on Twitter" class="twitter" target="_blank">Share on Twitter</a>
        <script>function fbs_click() {u=location.href;t=document.title;window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');return false;}</script><a href="http://www.facebook.com/share.php?u=<url>" onclick="return fbs_click()" class="facebook" target="_blank">Share on Facebook</a>
        
    </div>
