<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
	
<!--    <footer>
        <div class="wide-tab-container left-tab">
            <h3 class="right-header footer-header"><a href="/partners">Partners</a></h3>
            <div class="wide-right-tab right-tab"></div>
         </div>
        <div id="sponsors-container">
            <div id="main-sponsor">
                <h4>Major Partner</h4>
                <article>
                <a href="http://www.redenergy.com.au/efc"><img alt="Red, We're living energy Energy" src="<?php //bloginfo('template_directory'); ?>/images/red-energy.png" /></a>
                </article>
            </div>
            <div id="support-partners">
                <h4>Support Partners</h4>
                <article>
                    <a href="http://www.goodyearautocare.com.au/centric/gac2.jsp"><img alt="Good Year Autocare" src="<?php //bloginfo('template_directory'); ?>/images/good-year.png" /></a>
                </article>
                <article>
                    <a href="http://www.wolfblasswines.com/en/Country-Select.aspx?u=%2f/"><img alt="Wolf Blass" src="<?php //bloginfo('template_directory'); ?>/images/wolfblass.png" /></a>
                </article>
                <article>
                    <a href="http://www.edibleblooms.com.au/"><img alt="Edible Blooms" src="<?php //bloginfo('template_directory'); ?>/images/edible-blooms.png" /></a>
                </article>
                <article>
                    <a href="http://www.cachegroup.com/antler-luggage.php/"><img alt="Antler" src="<?php //bloginfo('template_directory'); ?>/images/antler.png" /></a>
                </article>
                <article class="no-margin">
                    <a href="http://www.southpacificmusical.com.au/"><img alt="South Pacific musical" src="<?php //bloginfo('template_directory'); ?>/images/south.png" /></a>
                </article>
            </div>
            
        </div>
    </footer>-->

</section><!-- end page container section -->

<?php wp_footer(); ?>
</body>
</html>
