<?php
/**
 * Template Name: Silent Auction Categories
 
 */

get_header(); ?>

<div class="main-content silent-cats auctions">
         
    <?php
        /* Run the loop to output the page.
        * If you want to overload this in a child theme then include a file
        * called loop-page.php and that will be used instead.
        */
        get_template_part( 'loop', 'page' );
    ?>
    
    <div id="silent-auction-cats">

         <?php	
                
         $parentCat = get_terms('auction_item');
        
         
        $count = count($parentCat);
        if($count > 0) {
            echo '<ul id="categories">';
            
            foreach ( $parentCat as $cat ) {
                if($cat->parent == '6') {
                    echo '<li><a href="/silent-auction-items/?cat=' . $cat->slug . '" title="' . $cat->name . '">' . $cat->name . '</a></li>';
                }
            }
            
            echo '</ul>';
        }
         
         
         

       ?>
            <a href="/auction" class="back-btn">Back to Auction</a>    
     </div>
        
                     
</div> 

    

		

<?php get_footer(); ?>
