<?php
/**
 * Template Name: Auctions Single Category
 
 */

get_header(); ?>

<div class="main-content auctions">
         
    
    
    <?php $category = $_GET['cat']; ?>
    
    <div class="tab-container left-tab">	
    <h3 class="right-header"><?php echo 'Auction Items - ' . ucwords($category); ?></h3>
    <div class="med-right-tab right-tab"></div>
    </div>

    <div class="cat cat-<?php echo $category ?>">
<!--        <a href="/silent-auctions" class="back-btn">Back to Categories</a>-->
        
     
         <?php	
                
        
        query_posts(array(  'post_type' => 'auction_items', 'caller_get_posts' => 1,  'auction_item' => $category, 'posts_per_page' => -1, 'meta_query' => array(array('key' => 'rrp' )), 'orderby'  => 'meta_value', 'meta_key' => 'rrp', 'order' => 'DESC'));
        
        

            
                if (have_posts()) : while (have_posts()) : the_post(); 
                
                $custom_field =  get_post_meta($post->ID);
                
                ?> 
        
                    <article>
                        <h4><a href="<?php echo $custom_field['url'][0] ?>"><?php echo the_title(); ?></a></h4>
                        <p><?php echo the_content(); ?></p>
                            
                        <label>RRP $<?php echo $custom_field['rrp'][0] ?></label>
                                          
                    </article>
                

                <?php endwhile; ?>
                <?php else : ?>
                <h5>No posts were found.</h5>
                <?php endif; ?>
         
         
         

       
           <a href="/silent-auctions" class="back-btn">Back to Categories</a>   
     </div>
        
                     
</div> 

    

		

<?php get_footer(); ?>
