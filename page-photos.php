<?php
/**
 * Template Name: Photos
 
 */

get_header(); ?>

<div class="main-content">
        
    <div id="photos">
    <?php
        /* Run the loop to output the page.
        * If you want to overload this in a child theme then include a file
        * called loop-page.php and that will be used instead.
        */
        get_template_part( 'loop', 'page' );
    ?>
           
     </div>
        
                     
</div> 

    

		

<?php get_footer(); ?>
