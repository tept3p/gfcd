<?php
/**
 * Template Name: Partners
 
 */

get_header(); ?>

<div class="main-content partners">
         
    <?php
        /* Run the loop to output the page.
        * If you want to overload this in a child theme then include a file
        * called loop-page.php and that will be used instead.
        */
        get_template_part( 'loop', 'page' );
    ?>
    
    <h2>Major Partner</h2>
    <a href="http://www.redenergy.com.au/efc"><img alt="red energy" src="/wp-content/themes/gfcd/images/partners-page/red.png" /></a>
    
    <h2>Support Partners</h2>
    <div class="float-logos">
        <ul>
            <li><a href="http://www.goodyearautocare.com.au/centric/gac2.jsp"><img alt="good year" src="/wp-content/themes/gfcd/images/partners-page/women.png" /></a></li> 
            <li><a href="http://www.wolfblasswines.com/en/Country-Select.aspx?u=%2f/"><img alt="wolf blass" src="/wp-content/themes/gfcd/images/partners-page/wolfblass.png" /></a></li> 
            <li><a href="http://www.southpacificmusical.com.au/"><img alt="south pacific" src="/wp-content/themes/gfcd/images/partners-page/south.png" /></a></li> 
            <li><a href="http://www.edibleblooms.com.au/"><img alt="edible blooms" src="/wp-content/themes/gfcd/images/partners-page/edible.png" /></a></li> 
            <li class="no-margin"><a href="http://www.cachegroup.com/antler-luggage.php/"><img alt="antler" src="/wp-content/themes/gfcd/images/partners-page/antler.png" /></a></li> 
        </ul>
        
    </div>
    
    <h2>Supply Partners</h2>
    <div class="float-logos">
        <ul>
            <li><a href="http://www.harveynorman.com.au" target="_blank"><img alt="harvey norman" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/harvey.png" /></a></li> 
            <li><a href="http://www.beiersdorf.com.au/" target="_blank"><img alt="beiesdorf" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/beindorph.png" /></a></li> 
            <li><a href="http://www.tupperware.com.au/wps/wcm/connect/aus/website" target="_blank"><img alt="tuppaware" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/tupperware.png" /></a></li> 
            <li><a href="http://www.goldwell.com.au/sites/public/default.aspx" target="_blank"><img alt="goldwell" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/GW-logo.png" /></a></li> 
            <li class="no-margin"><a href="http://www.foodforhealth.com.au/" target="_blank"><img alt="food for health" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/food.png" /></a></li> 
            
            <li><a href=""><img alt="columbine" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/columbine.png" /></a></li> 
            <li><a href="http://www.jellybelly.com.au/" target="_blank"><img alt="jelly belly" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/jelly.png" /></a></li>
            <li><a href=""><img alt="woody allen to rome with love" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/woody.png" /></a></li> 
            <li><a href="http://egopharm.com/" target="_blank"><img alt="ego" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/ego.png" /></a></li> 
            <li class="no-margin"><a href="http://www.nivea.com.au/" target="_blank"><img alt="nivea" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/nivea.png" /></a></li> 
            
            <li><a href="http://www.oceanspray.com.au/" target="_blank"><img alt="ocean spray" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/ocean.png" /></a></li> 
            <li><a href="http://www.tomorganic.com.au/" target="_blank"><img alt="tom organic" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/tom.png" /></a></li> 
            <li><a href="http://www.meccacosmetica.com.au/" target="_blank"><img alt="mecca" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/mecca.png" /></a></li> 
            <li class="no-margin"><a href="http://www.tbn.com.au/c4/designer-brands/" target="_blank"><img alt="designer brands" src="/wp-content/themes/gfcd/images/partners-page/supply_partners/deb.png" /></a></li>
        </ul>
        
    </div>
    
    <div class="single-partner">
        <h2>Charity Partner</h2>
        <a href="http://www.osteoporosis.org.au"><img alt="Osteoporosis Australia" src="/wp-content/themes/gfcd/images/partners-page/oa_logo.png" /></a>
    </div>    
    
    <div class="afl-ewn">
        <h2>Proudly presented by</h2>
        <a href="http://www.essendonfc.com.au/supporters/coteries.asp"><img alt="essendon women's network" src="/wp-content/themes/gfcd/images/partners-page/ewn.png" /></a>
    </div>
    
    <div class="afl-ewn">
        <h2>Endorsed by</h2>
        
        <a href="http://www.afl.com.au/" class="no-margin"><img alt="afl" src="/wp-content/themes/gfcd/images/partners-page/afl.png" /></a>
    </div>
</div> 

    

		

<?php get_footer(); ?>
