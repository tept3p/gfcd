<?php
/**
 * Template Name: Auctions
 
 */

get_header(); ?>

<div class="main-content auctions">
         
    <?php
        /* Run the loop to output the page.
        * If you want to overload this in a child theme then include a file
        * called loop-page.php and that will be used instead.
        */
        get_template_part( 'loop', 'page' );
    ?>    
 	
    
    <!-- Uncomment all the below content to add the auctions back onto theis landing page -->
    
    
<!--    
    <?php   
    
    //query_posts(array(  'post_type' => 'auction_items', 'caller_get_posts' => 1,  'auction_item' => 'live-auction', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC', ));    
        //if (have_posts()) : while (have_posts()) : the_post(); 
        
    ?>
    
    <div class="live item">
        <h2><a href="/live-auction" title="see live auctions">Live Auction</a></h2>
        <p>- Here is one of the items from our Live Auction</p>
        <h3><?php //echo the_title(); ?></h3>
        <?php //$custom_field =  get_post_meta($post->ID); ?>
        <label>RRP $<?php //echo $custom_field['rrp'][0]; ?></label>
        <a href="/live-auction" class="see-more" alt="" title="">View more from our Live Auction<img src="<?php //bloginfo('template_directory'); ?>/images/more-arrow.png" /></a>
    </div>
    
    
    
    <?php //endwhile; ?>
    <?php //else : ?>
    <h5>No posts were found.</h5>
    <?php //endif; 
        
    //wp_reset_query(); 
    
    
       // query_posts(array(  'post_type' => 'auction_items', 'caller_get_posts' => 1,  'auction_item' => 'silent-auction', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC', ));    
        //if (have_posts()) : while (have_posts()) : the_post(); 
        
    ?>
    
    <div class="silent item">
        <h2><a href="/silent-auctions">Silent Auction</a></h2>
        <p>- Here is one of the items from our Silent Auction Category</p>
        <h3><?php //echo the_title(); ?></h3>
         
        <p>
        <?php 
        //$categories = wp_get_post_terms($post->ID, 'auction_item');
        //foreach($categories as $cat) {
         //   $cats[] = $cat->slug;
        //}
        
        //echo the_content();
        ?>
        </p>
        <?php //$custom_field =  get_post_meta($post->ID); ?>
        <label>RRP $<?php //echo $custom_field['rrp'][0]; ?></label>
        <?php
        //if(in_array('silent-auction', $cats)) { ?>
            <a href="/silent-auctions" class="see-more" alt="" title="">View more items<img src="<?php// bloginfo('template_directory'); ?>/images/more-arrow.png" /></a>
        <?php //}?>
    </div>

        
    <?php //endwhile; ?>
    <?php //else : ?>
    <h5>No posts were found.</h5>
    <?php //endif; ?>
    <?php //wp_reset_query();
    
    //query_posts(array(  'post_type' => 'auction_items', 'caller_get_posts' => 1,  'auction_item' => 'raffle', 'posts_per_page' => 1, 'orderby' => 'rand', 'order' => 'ASC', ));    
        //if (have_posts()) : while (have_posts()) : the_post(); 
        
    ?>
    
<div class="raffle item">    
        <h2><a href="/raffle" title="see raffle items">Raffle</a></h2>
        <p>- Here is one of the items from our Raffle</p>
        <h3><?php //echo the_title(); ?></h3>
        <p><?php //echo the_content(); ?></p>
        <?php //$custom_field =  get_post_meta($post->ID); ?>
        <label>RRP $<?php //echo $custom_field['rrp'][0]; ?></label>
        <a href="/raffle" class="see-more" alt="" title="">View more of our Raffle prizes<img src="<?php //bloginfo('template_directory'); ?>/images/more-arrow.png" /></a>
    </div>
    
    
    <?php //endwhile; ?>
    <?php //else : ?>

    <?php //endif; ?>
   


-->

</div>

<?php get_footer(); ?>
