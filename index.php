<?php
/**
 * Template Name: Home
 
 */

get_header(); ?>

<div class="main-content home">
    
        <div class="tab-container left-tab">
            <h3 class="right-header">Home</h3>
            <div class="med-right-tab right-tab"></div>
        </div>
       <p>The Grand Final Comedy Debate is the annual premier AFL endorsed event for women during Grand Final week.</p><p> Held at Crown’s Palladium on Thursday 27th September, leading Australian comedians including FOX FM’s morning show hosts <a href="http://grandfinalcomedydebate.bastionserve.net/2012/08/24/matt-tiley/">Matt Tilley</a> and <a href="http://grandfinalcomedydebate.bastionserve.net/2012/08/24/jo-stanley/">Jo Stanley</a>, Gold FM’s <a href="http://grandfinalcomedydebate.bastionserve.net/2012/08/28/anthony-lehmo-lehmann/">Anthony “Lehmo” Lehmann</a>, <a href="http://grandfinalcomedydebate.bastionserve.net/2012/08/24/fiona-oloughlin/">Fiona O’Loughlin</a>, <a href="http://grandfinalcomedydebate.bastionserve.net/2012/08/24/matthew-hardy/">Matthew Hardy</a> and Herald on Sunday football writer and Fox presenter <a href="http://grandfinalcomedydebate.bastionserve.net/2012/08/28/mark-robinson/">Mark Robinson</a> will debate the topic:</p>
          <h4>“THAT THERE ARE NO GENTLEMAN LEFT IN FOOTBALL”</h4>
          
          <p>The event boasts a large silent and main auction segment which raises funds, towards a leading charity. A highlight of the lunch is the announcement and presentation of the coveted annual AFL Football Woman of the Year Award. Special performance from the cast from the Tony Award winning musical ‘South Pacific’.</p>  
          <img src="http://grandfinalcomedydebate.bastionserve.net/files/2012/08/home-img.jpg" alt="" title="comedians-img-home" width="507" height="288" class="alignnone size-full wp-image-124" />
    
    
          <div class="tab-container left-tab">
            <h3 class="right-header">Newsletter</h3>
            <div class="med-right-tab  right-tab"></div>
         </div>   
          
         <p>Want to receive <span>updates?</span> Please enter your email here</p>
        <form action="http://bastion.createsend.com/t/j/s/jyhjju/" method="post" id="subForm">
            <div>
            <label for="name">Name:</label><input type="text" name="cm-name" value="Your Name" id="name" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'Your Name':this.value;" />
            <label for="jyhjju-jyhjju">Email:</label><input type="text" value="Your Email" name="cm-jyhjju-jyhjju" id="jyhjju-jyhjju" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'Your Email':this.value;" />

            <input class="submit" type="submit" value="Submit" />
            </div>
        </form> 
                     
</div> 

    

		

<?php get_footer(); ?>
