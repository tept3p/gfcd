<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="tab-container left-tab">	
                                        <h3 class="right-header">
                                        <?php if(get_query_var('name') == 'mark-robinson') { echo 'Media Personality'; } else if(get_query_var('name') == 'belinda-duarte') { echo 'Award Winner'; } else { $category = get_the_category();
$firstCategory = $category[0]->cat_name; echo $firstCategory; } ?> - <?php the_title(); ?></h3>
                                        <div class="med-right-tab right-tab"></div>
                                        </div>

                                    
                                        <?php echo the_content(); ?>
                                        
                                    

                                </div>

<?php endwhile; // end of the loop. ?>
