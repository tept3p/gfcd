<?php
/**
 * The loop that displays a page.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-page.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php if ( is_front_page() ) { ?>
					<div class="tab-container left-tab">	
                                        <h3 class="right-header"><?php the_title(); ?></h3>
                                        <div class="med-right-tab right-tab"></div>
                                        </div>
					<?php } else if($post->post_name == 'silent-auction-items') { ?>
						<div class="tab-container left-tab">	
                                                <h3 class="right-header"><?php the_title() . ' - ' . $_GET['cat']; ?></h3>
                                                <div class="med-right-tab right-tab"></div>
                                                </div>
					<?php } else { ?>
                                                <div class="tab-container left-tab">	
                                                <h3 class="right-header"><?php the_title(); ?></h3>
                                                <div class="med-right-tab right-tab"></div>
                                                </div>
                                        <?php } ?>

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
						<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
					</div><!-- .entry-content -->
				</div><!-- #post-## -->

<?php endwhile; // end of the loop. ?>
